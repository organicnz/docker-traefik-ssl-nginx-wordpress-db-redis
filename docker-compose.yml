version: '3'

networks:
    # enable connection with Traefik
    traefik:
      external: true
    internal:
      external: false
      driver: bridge
    # network for the app
    backend:

services:

  traefik:
    image: "traefik:v2.2"
    container_name: "traefik"
    restart: unless-stopped
    command:
      # Only for development environment
      - "--log.level=DEBUG"
      - "--api.insecure=true"
      # Get Docker as the provider
      - "--providers.docker=true"
      # Avoid that all containers are exposed
      - "--providers.docker.exposedbydefault=false"
      # Settle the ports for the entry points
      - "--entrypoints.web.address=:80"
      - "--entrypoints.web-secure.address=:443"
      # Settle the autentification method to http challenge
      - "--certificatesresolvers.myhttpchallenge.acme.httpchallenge=true"
      - "--certificatesresolvers.myhttpchallenge.acme.httpchallenge.entrypoint=web"
      # Uncomment this to get a fake certificate when testing
      #- "--certificatesresolvers.myhttpchallenge.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory"
      # Settle letsencrypt as the certificate provider
      - "--certificatesresolvers.myhttpchallenge.acme.email=tamerlanium@gmail.com"
      - "--certificatesresolvers.myhttpchallenge.acme.storage=/letsencrypt/acme.json"
    ports:
      - "80:80"
      - "443:443"
      - "8080:8080"
    labels:
      - "traefik.backend=traefik"
      - "traefik.frontend.rule=Host(`thehybrid.xyz`)"
      - "traefik.port=8080"
    networks:
      - "traefik"
    volumes:
      # Store certificates in ./letsencrypt/acme.json
      - "./letsencrypt:/letsencrypt"
      # Connect to Doker socket
      - "/var/run/docker.sock:/var/run/docker.sock:ro"

  nginx-foodhsare:
    container_name: nginx-foodhsare
    image: nginx:alpine
    restart: unless-stopped
    ports:
      - '80'
    volumes:
      - /foodshare/nginx-wp1:/etc/nginx/conf.d
      - /logs/nginx-wp1:/var/log/nginx
      - /foodshare/data:/var/www/html
    labels:
      - "traefik.backend=nginxwp1"
      - "traefik.frontend.rule=Host(`foodshare.club`,`www.foodshare.club`)"
      - "traefik.docker.network=traefik"
      # Not recommended
      #- traefik.port=8081
    networks:
      - traefik
      - internal
    depends_on:
      - wordpress-foodshare
        
  wordpress-foodshare:
    container_name: wordpress-foodshare         
    build:
      # call the Dockerfile in ./wordpress
      context: ./foodshare
    restart: unless-stopped
    environment:
      # Connect WordPrerss to the database $MYSQL_USER
      - WORDPRESS_DB_HOST=db-foodshare:3306
      - WORDPRESS_DB_USER=$WORDPRESS_DB_USER
      - WORDPRESS_DB_PASSWORD=$WORDPRESS_DB_PASSWORD
      - WORDPRESS_DB_NAME=$WORDPRESS_DB_NAME_FOODSHARE
    volumes:
      # save the content of WordPress an enable local modifications
      - ./foodshare/data:/var/www/html
    networks:
      - traefik
      - backend
    depends_on:
        - db-foodshare
        - redis
    labels:
      # The labels are usefull for Traefik only
      - "traefik.enable=true"
      - "traefik.docker.network=traefik"
      # Get the routes from http
      - "traefik.http.routers.wordpresscp.rule=Host(`foodshare.club`)"
      - "traefik.http.routers.wordpresscp.entrypoints=web"
      # Redirect these routes to https
      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https"
      - "traefik.http.routers.wordpresscp.middlewares=redirect-to-https@docker"
      # Get the routes from https
      - "traefik.http.routers.wordpresscp-secured.rule=Host(`foodshare.club`)"
      - "traefik.http.routers.wordpresscp-secured.entrypoints=web-secure"
      # Apply autentificiation with http challenge
      - "traefik.http.routers.wordpresscp-secured.tls=true"
      - "traefik.http.routers.wordpresscp-secured.tls.certresolver=myhttpchallenge"

  db-foodshare:
    # this is the database used by Wordpress        
    container_name: db-foodshare    
    image: mysql:8.0.23
    restart: unless-stopped
    environment:
      # Connect WordPrerss to the database
      - MYSQL_DATABASE=$MYSQL_DATABASE_NAME_FOODSHARE
      - MYSQL_USER=$MYSQL_USER
      - MYSQL_PASSWORD=$MYSQL_PASSWORD
      - MYSQL_RANDOM_ROOT_PASSWORD='1'
    volumes:
      # Persist the database on disk
      - ./db_foodshare:/var/lib/mysql
    networks:
      - backend

  redis:
    container_name: redis
    image: redis:6
    restart: unless-stopped
    ports:
      - "6379:6379"
    networks:
      - backend
    # launch Redis in cache mode with :
    #  - max memory up to 50% of your RAM if needed (--maxmemory 1024mb)
    #  - deleting oldest data when max memory is reached (--maxmemory-policy allkeys-lru)
    entrypoint: redis-server --maxmemory 1024mb --maxmemory-policy allkeys-lru

volumes:
  data:
  db-foodshare:
  letsencrypt:

