# Combined docker-compose based on tutorials below

[A Docker Compose file to install WordPress with a Traefik reverse proxy, an SSL certificate and a Redis cache…](https://thibaut-deveraux.medium.com/a-docker-compose-file-to-install-wordpress-with-a-traefik-reverse-proxy-an-ssl-certificate-and-a-e878c2a03a17).

[Building a twin Wordpress stack with Traefik, Nginx and Docker](https://techroads.org/building-twin-wordpress-traefik-nginx-docker/).

## TODO: I want to solve an issue running multiple Nginx, Wordpress and DB containers.

### Open issue on Stackoverflow

[404 page not found on multi-site Docker](https://stackoverflow.com/questions/66053322/404-page-not-found-on-multi-site-docker).
