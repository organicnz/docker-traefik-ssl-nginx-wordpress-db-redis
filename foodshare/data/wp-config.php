<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'foodshare');

/** MySQL database username */
define( 'DB_USER', 'organic');

/** MySQL database password */
define( 'DB_PASSWORD', 'XmrkLB9qYR4WmgbQ');

/** MySQL hostname */
define( 'DB_HOST', 'db-foodshare:3306');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '63672cb2580eb676665ee611ab45ca2a6caae955');
define( 'SECURE_AUTH_KEY',  'f657cfc64aa62facdd5d1244938516a987042054');
define( 'LOGGED_IN_KEY',    'd103a5eb5b1b545422bca64252fa3f8f89187396');
define( 'NONCE_KEY',        '530f26c8ca8c8f4d6c042cd8af75121c33a549c6');
define( 'AUTH_SALT',        'd1f2582b3f284691e3187fb9436500a769566cf8');
define( 'SECURE_AUTH_SALT', 'c9f0ca345f2aa3b7748c237c12a478391952a652');
define( 'LOGGED_IN_SALT',   '628526a87575ed83f5d3dffc8ce38434de8e94f3');
define( 'NONCE_SALT',       'fcc2c3521ae1b5fcdb79f9310365cd147a0b7747');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

// If we're behind a proxy server and using HTTPS, we need to alert WordPress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
